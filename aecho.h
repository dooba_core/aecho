/* Dooba SDK
 * Aecho MP3 Player Module Driver
 */

#ifndef	__AECHO_H
#define	__AECHO_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <vs1011/vs1011.h>

// States
#define	AECHO_STOPPED							0
#define	AECHO_PLAYING							1
#define	AECHO_PAUSED							2

// Buffer Size
#ifndef	AECHO_BUFSIZE
#define	AECHO_BUFSIZE							512
#endif

// Buffer Fill Threshold
#define	AECHO_BUF_FILL_THRESHOLD				(AECHO_BUFSIZE / 8)

// Max Volume
#define	AECHO_VOL_MAX							VS1011_VOL_MAX

// Aecho Structure
struct aecho
{
	// Decoder
	struct vs1011 *dec;

	// State
	uint8_t state;

	// Current File
	struct vfs_handle file;

	// Play Progress
	uint32_t fsize;
	uint32_t fpos;
	uint8_t fpct;

	// Volume
	uint8_t vol;

	// Play Buffer
	uint8_t buffer[AECHO_BUFSIZE];
	uint16_t buffer_rdpos;
	uint16_t buffer_wrpos;

	// End-Of-File Callback
	void (*eof)(void *user);

	// User Data
	void *user;
};

// Initialize MP3 Subsystem
extern void aecho_init(struct aecho *a, struct vs1011 *decoder);

// Update MP3 Subsystem
extern void aecho_update(struct aecho *a);

// Play File
extern uint8_t aecho_play(struct aecho *a, char *file, void (*eof_callback)(void *user), void *user);

// Play File - Fixed Length
extern uint8_t aecho_play_n(struct aecho *a, char *file, uint8_t file_len, void (*eof_callback)(void *user), void *user);

// Pause currently playing file
extern void aecho_pause(struct aecho *a);

// Resume playing
extern void aecho_resume(struct aecho *a);

// Toggle Pause / Resume
extern void aecho_toggle_pause(struct aecho *a);

// Stop
extern void aecho_stop(struct aecho *a);

// Set Volume
extern void aecho_set_vol(struct aecho *a, uint8_t vol);

// Get Volume
extern uint8_t aecho_get_vol(struct aecho *a);

// Get Volume as Percentage
extern uint8_t aecho_get_vol_pct(struct aecho *a);

// Volume Up
extern void aecho_vol_up(struct aecho *a);

// Volume Up - Multiple
extern void aecho_vol_up_x(struct aecho *a, uint8_t x);

// Volume Down
extern void aecho_vol_down(struct aecho *a);

// Volume Down - Multiple
extern void aecho_vol_down_x(struct aecho *a, uint8_t x);

#endif
