/* Dooba SDK
 * Aecho MP3 Player Module Driver
 */

// External Includes
#include <string.h>
#include <stdlib.h>
#include <util/delay.h>
#include <vfs/vfs.h>

// Internal Includes
#include "aecho.h"

// Initialize
void aecho_init(struct aecho *a, struct vs1011 *decoder)
{
	// Set Decoder
	a->dec = decoder;

	// Clear EoF Callback
	a->eof = (void *)0;

	// Set Stopped
	a->state = AECHO_STOPPED;

	// Clear Buffer
	a->buffer_rdpos = 0;
	a->buffer_wrpos = 0;

	// Clear Progress
	a->fsize = 0;
	a->fpos = 0;
	a->fpct = 0;

	// Zero Volume
	a->vol = 0;
}

// Update MP3 Subsystem
void aecho_update(struct aecho *a)
{
	uint16_t r;
	uint16_t rd;
	void (*p)(void *user);

	// Check Playing
	if(a->state != AECHO_PLAYING)														{ return; }

	// Acquire Callback
	p = a->eof;

	/* First Part - Read MP3 Data from File
	 */

	// Determine Write Size
	if(a->buffer_rdpos <= a->buffer_wrpos)												{ r = AECHO_BUFSIZE - a->buffer_wrpos; }
	else																				{ r = (a->buffer_rdpos - a->buffer_wrpos) - 1; }

	// Check Write Size
	if((r) && ((r >= AECHO_BUF_FILL_THRESHOLD) || (a->buffer_rdpos <= a->buffer_wrpos)))
	{
		// Read a chunk of data
		if(r > AECHO_BUF_FILL_THRESHOLD)												{ r = AECHO_BUF_FILL_THRESHOLD; }
		if(vfs_read(&a->file, &(a->buffer[a->buffer_wrpos]), r, &rd))					{ rd = 0; }
		else																			{ a->buffer_wrpos = a->buffer_wrpos + rd; }

		// Detect End-Of-File
		if(rd == 0)																		{ if(p) { a->eof = 0; p(a->user); } }

		// Yield
		return;
	}

	// Loop Buffer when appropriate (always keep wrpos _behind_ rdpos)
	if((a->buffer_wrpos >= AECHO_BUFSIZE) && (a->buffer_rdpos))							{ a->buffer_wrpos = 0; return; }

	/* Second Part - Push MP3 Data to Decoder
	 */

	// Determine how much data we can push to the decoder
	if(a->buffer_rdpos < a->buffer_wrpos)												{ r = (a->buffer_wrpos - a->buffer_rdpos) - 1; }
	else if(a->buffer_rdpos > a->buffer_wrpos)											{ r = AECHO_BUFSIZE - a->buffer_rdpos; }
	else																				{ return; }

	// Push data to decoder
	rd = vs1011_send_data(a->dec, &(a->buffer[a->buffer_rdpos]), r);
	a->buffer_rdpos = a->buffer_rdpos + rd;
	if(a->buffer_rdpos >= AECHO_BUFSIZE)												{ a->buffer_rdpos = 0; }

	// Update Progress
	a->fpos = a->fpos + rd;
	a->fpct = (a->fpos * 100) / a->fsize;
}

// Play File
uint8_t aecho_play(struct aecho *a, char *file, void (*eof_callback)(void *user), void *user)
{
	// Play
	return aecho_play_n(a, file, strlen(file), eof_callback, user);
}

// Play File - Fixed Length
uint8_t aecho_play_n(struct aecho *a, char *file, uint8_t file_len, void (*eof_callback)(void *user), void *user)
{
	// Stop
	aecho_stop(a);

	// Open File
	if(vfs_open_n(&a->file, file, file_len, 0))											{ return 1; }

	// Set User Data
	a->user = user;

	// Check Regular File
	if(a->file.otype == VFS_OTYPE_FILE)
	{
		// Determine File size
		a->fsize = a->file.size;
		a->fpos = 0;
		a->fpct = 0;

		// Enter Play State
		a->state = AECHO_PLAYING;
		a->buffer_rdpos = 0;
		a->buffer_wrpos = 0;

		// Set EOF Callback
		a->eof = eof_callback;
	}
	else
	{
		// Abort
		vfs_close(&a->file);
		eof_callback(user);
	}

	// Done
	return 0;
}

// Pause currently playing file
void aecho_pause(struct aecho *a)
{
	// Check Playing
	if(a->state != AECHO_PLAYING)														{ return; }

	// Set Paused
	a->state = AECHO_PAUSED;
}

// Resume playing
void aecho_resume(struct aecho *a)
{
	// Check Paused
	if(a->state != AECHO_PAUSED)														{ return; }

	// Set Playing
	a->state = AECHO_PLAYING;
}

// Toggle Pause / Resume
void aecho_toggle_pause(struct aecho *a)
{
	// Check Playing
	if(a->state == AECHO_PLAYING)														{ aecho_pause(a); }
	else																				{ aecho_resume(a); }
}

// Stop
void aecho_stop(struct aecho *a)
{
	// Check Stopped { Close Previous File } (Ensure Stopped)
	if(a->state != AECHO_STOPPED)														{ vfs_close(&a->file); }
	a->state = AECHO_STOPPED;
	a->fsize = 0;
	a->fpos = 0;
	a->fpct = 0;

	// Reset Play Buffer
	a->buffer_rdpos = 0;
	a->buffer_wrpos = 0;
}

// Set Volume
void aecho_set_vol(struct aecho *a, uint8_t vol)
{
	// Set Volume
	if(vol > AECHO_VOL_MAX)																{ vol = AECHO_VOL_MAX; }
	a->vol = vol;

	// Set Volume
	vs1011_set_vol(a->dec, a->vol, a->vol);
}

// Get Volume
uint8_t aecho_get_vol(struct aecho *a)
{
	// Volume
	return a->vol;
}

// Get Volume as Percentage
uint8_t aecho_get_vol_pct(struct aecho *a)
{
	uint16_t x;
	x = a->vol;
	x = x * 10;
	x = x / AECHO_VOL_MAX;
	x = x * 10;
	return x;
}

// Volume Up
void aecho_vol_up(struct aecho *a)
{
	// Check Volume
	if(a->vol >= AECHO_VOL_MAX)															{ return; }

	// Inc Volume
	a->vol = a->vol + 1;

	// Set Volume
	vs1011_set_vol(a->dec, a->vol, a->vol);
}

// Volume Up - Multiple
void aecho_vol_up_x(struct aecho *a, uint8_t x)
{
	// Up
	while(x)																			{ aecho_vol_up(a); x = x - 1; }
}

// Volume Down
void aecho_vol_down(struct aecho *a)
{
	// Check Volume
	if(a->vol == 0)																		{ return; }

	// Dec Volume
	a->vol = a->vol - 1;

	// Set Volume
	vs1011_set_vol(a->dec, a->vol, a->vol);
}

// Volume Down - Multiple
void aecho_vol_down_x(struct aecho *a, uint8_t x)
{
	// Down
	while(x)																			{ aecho_vol_down(a); x = x - 1; }
}
